Just changes timezone to Atlantic Canada

ENV TZ=America/Moncton
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
